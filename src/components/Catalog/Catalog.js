import React from 'react';

const CatalogItem = (props) => {
  console.log('Catalog--Item: ', props);
  return (
    <article className="catalog--item">
      <a>
        <picture>
          <img src="#" alt="alt" />
        </picture>
      </a>
      <h3>name</h3>
      <p>$999.00</p>
    </article>
  );
};

const CatalogList = (props) => {
  console.log('Catalog--List: ', props);
  return (
    <section className="catalog--list">
      <CatalogItem />
    </section>
  );
};


const Catalog = () => {
  console.log('catalogStore: ');
  return (
    <main>
      <CatalogList />
    </main>
  );
};

export default Catalog;
