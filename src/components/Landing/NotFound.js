import React from 'react';

const NotFound = () => (
  <header className="App-header">
    <h1>ERROR 404</h1>
  </header>
);

export default NotFound;
