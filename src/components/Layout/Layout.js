import React, { Component } from 'react';
import { withRouter } from 'react-router';

class Layout extends Component {
  state = {
    ui: {},
  }

  // componentDidMount() {
  //   console.log('-Layout- this: ', this);
  // }

  // componentDidUpdate() {
  //   const { history } = this.props;
  //   console.log('-Layout- history: ', history.location.pathname);
  // }

  render() {
    const { children } = this.props;
    return (
      <div id="layout" className="layout">
        {children}
      </div>
    );
  }
}

export default withRouter(Layout);
