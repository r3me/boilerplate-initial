import React from 'react';
import { NavLink } from 'react-router-dom';

import { categoryStore } from '../../../stores/categoryStore';

const Nav = () => (
  <nav>
    <NavLink activeClassName="active-nav" to="/" exact>Home</NavLink>
    <NavLink activeClassName="active-nav" to="/catalog">Catalog</NavLink>
    <NavLink activeClassName="active-nav" to="/product/test">Product ID</NavLink>
    <NavLink activeClassName="active-nav" to="/checkout">Checkout</NavLink>
    <NavLink activeClassName="active-nav" to="/custom/">Custom</NavLink>
    <NavLink activeClassName="active-nav" to="/blank/">Blank</NavLink>
    <NavLink activeClassName="active-nav" to="/nyan/">404</NavLink>
    {console.log('categories: ', categoryStore())}
  </nav>
);

export default Nav;
