import React from 'react';
import { Route, Link } from 'react-router-dom';

const LayoutBlank = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={matchProps => (
      <div className="layout--blank">
        <Link to="/">« volver</Link>
        <Component {...matchProps} />
      </div>
    )}
  />
);

export default LayoutBlank;
