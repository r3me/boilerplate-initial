import React from 'react';
import { Route } from 'react-router-dom';

import Nav from './Nav/Nav';

const LayoutDefault = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={matchProps => (
      <div className="layout--default">
        <Nav />
        <h1>Default</h1>
        <Component {...matchProps} />
      </div>
    )}
  />
);

export default LayoutDefault;
