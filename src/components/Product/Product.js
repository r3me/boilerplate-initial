import React from 'react';

const Product = (props) => {
  const { match } = props;
  const name = match.params.id;
  return (
    <main>
      <h1>Product is: {name}</h1>
      {console.log('Product: ', props)}
    </main>
  );
};

export default Product;
