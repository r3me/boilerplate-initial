import React, { Component } from 'react';
import _ from 'lodash';

import '../sass/demo/infinite-scroll.scss';

const LoadingComponent = () => (
  <article className="scroll--item">
    <picture>
      <img src="/media/demo/loading.gif" alt="placeholder" />
    </picture>
    <header>
      <h3>Product name</h3>
      <p>$999.00</p>
    </header>
  </article>
);

const Loading = ({ limit }) => {
  const items = [];
  _.times(limit, (i) => {
    items.push(<LoadingComponent key={i} />);
  });
  return items;
};

class InfiniteScroll extends Component {
  state = {
    data: null,
    error: null,
    pages: null,
    page: 1,
    limit: 30,
    returned: 30,
    products: [],
    next: [],
  }

  componentDidMount() {
    const { page } = this.state;
    this.initialRequest(page);
  }

  initialRequest = async (page) => {
    this.setState({ data: null, error: null });
    const { limit } = this.state;
    const merchant = 'chapurmx';
    const api = `https://${merchant}.wcaas.net/api/wcaas/mongoApi/${merchant}/page/productsByCategory/`;
    const category = 'moda-mujer/ropa/blusas-y-playeras';
    const params = `${api}${category}?lang=es_mx&currency=mxn&page=${page}&limit=${limit}`;
    try {
      const url = params;
      const req = await fetch(url);
      const data = await req.json();
      const products = data.productData;
      const totalProducts = parseFloat(data.totalProducts.replace(',', ''));
      const pages = (totalProducts / limit);
      const totalPages = (pages > pages.toFixed()) ? pages.toFixed() + 1 : pages.toFixed();
      this.setState({ data, products, pages: parseFloat(totalPages) });
      this.renderNext(products, limit);
    } catch (err) {
      this.setState({ error: err });
    } finally {
      console.log('scrollRequest is done -- ', this.state);
    }
  }

  scrollRequest = async (page) => {
    this.setState({ data: null, error: null });
    const { limit } = this.state;
    const merchant = 'chapurmx';
    const api = `https://${merchant}.wcaas.net/api/wcaas/mongoApi/${merchant}/page/productsByCategory/`;
    const category = 'moda-mujer/ropa/blusas-y-playeras';
    const params = `${api}${category}?lang=es_mx&currency=mxn&page=${page}&limit=${limit}`;
    try {
      const url = params;
      const req = await fetch(url);
      const data = await req.json();
      const products = data.productData;
      const totalProducts = parseFloat(data.totalProducts.replace(',', ''));
      const pages = (totalProducts / limit);
      const totalPages = (pages > pages.toFixed()) ? pages.toFixed() + 1 : pages.toFixed();
      this.setState({ data, products, pages: parseFloat(totalPages) });
    } catch (err) {
      this.setState({ error: err });
    } finally {
      console.log('scrollRequest is done -- ', this.state);
    }
  }

  scrollNext = async () => {
    const { data, page, pages, returned, products, limit } = this.state;
    const nextPage = (page + 1);
    const newProducts = products !== [] ? products : [];
    const returnedProducts = returned + parseFloat(data.returnedProducts);
    const lastPage = page === pages;
    // const totalPages = (totalProducts / limit);
    const scroller = document.getElementById('scroller');
    const bounding = scroller.getBoundingClientRect();
    console.log('bounding: ', bounding);
    if (!lastPage) {
      this.setState({ page: nextPage, returned: returnedProducts });
      this.scrollRequest(nextPage);
      this.renderNext(newProducts, limit);
    } else {
      this.setState({ page, returned: returnedProducts });
      this.renderNext(newProducts, limit);
    }
  }

  renderNext = (newProducts, limit) => {
    const { next } = this.state;
    const ready = newProducts !== null && newProducts !== [];
    const cdn = 'https://cdn.chapur.com.mx';
    const list = ready ? newProducts.map(i => (
      <article key={i.part_number} className="scroll--item">
        <picture>
          <img src={`${cdn}/${i.part_number}/${i.items[0].image_correlator}_1_500x500.jpg`} alt={i.items[0].locales.es_mx.name} />
        </picture>
        <header>
          <h3>{i.items[0].locales.es_mx.name}</h3>
          <p>{`$${i.items[0].pricing.mxn.list_price / 100}.00`}</p>
        </header>
      </article>
    )) : <Loading limit={limit} />;
    next.push(list);
  }

  render() {
    const { data, page, returned, products, limit, next, error } = this.state;
    const cdn = 'https://cdn.chapur.com.mx';

    const err = error !== null ? <h4>{error}</h4> : '';
    const ready = data !== null && products !== null;
    const totalProducts = ready && data.totalProducts ? parseFloat(data.totalProducts.replace(',', '')) : 'loading...';
    let totalReturned;
    if (returned > totalProducts) {
      totalReturned = (returned - (returned - totalProducts));
    } else { totalReturned = returned; }
    const totalPages = (totalProducts / limit);
    const pageCount = `${page} / ${totalPages > parseFloat(totalPages.toFixed()) ? parseFloat(totalPages.toFixed()) + 1 : parseFloat(totalPages.toFixed())}`;

    const scroller = document.getElementById('scroller');
    const intersectionObserverOptions = {
      root: document.getElementById('layout'),
      rootMargin: '0px',
      threshold: 1,
    };
    function callback(entries) {
      if (entries[0].isIntersecting) {
        this.scrollNext();
      }
    }
    const observer = new IntersectionObserver(callback, intersectionObserverOptions);
    observer.observe(scroller);

    const list = ready ? products.map(i => (
      <article key={i.part_number} className="scroll--item">
        <picture>
          <img src={`${cdn}/${i.part_number}/${i.items[0].image_correlator}_1_500x500.jpg`} alt={i.items[0].locales.es_mx.name} />
        </picture>
        <header>
          <h3>{i.items[0].locales.es_mx.name}</h3>
          <p>{`$${i.items[0].pricing.mxn.list_price / 100}.00`}</p>
        </header>
      </article>
    )) : <Loading limit={limit} />;

    return (
      <main>
        <header className="scroll--header">
          <h1>Catalog Infinite Scroll</h1>
          <div>
            <p>currentPage: </p>
            <b>{pageCount}</b>
          </div>
          <div>
            <p>totalProducts: </p>
            <b>{totalProducts}</b>
          </div>
          <div>
            <p>returnedProducts: </p>
            <b>{totalReturned}</b>
          </div>
        </header>
        <div className="scroll--nav">
          <b>{pageCount}</b>
        </div>
        {err}
        <section className="scroll--content">
          {list}
          {next}
        </section>
        <a id="scroller">MORE</a>
      </main>
    );
  }
}

export default InfiniteScroll;
