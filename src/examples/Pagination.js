import React, { Component } from 'react';
import _ from 'lodash';

import '../sass/demo/pagination.scss';

const PlaceholderItem = () => (
  <article className="pagination--item">
    <picture>
      <img src="/media/demo/loading.gif" alt="placeholder" />
    </picture>
    <header>
      <h3>Product name</h3>
      <p>$999.00</p>
    </header>
  </article>
);

const PlaceholderList = ({ limit }) => {
  const items = [];
  _.times(limit, (i) => {
    items.push(<PlaceholderItem key={i} />);
  });
  return items;
};

class Pagination extends Component {
  state = {
    data: null,
    error: null,
    pages: null,
    page: 1,
    limit: 300,
    returned: 300,
    products: [],
  }

  componentDidMount() {
    const { page } = this.state;
    this.pageRequest(page);
  }

  pageRequest = async (page) => {
    this.setState({ data: null, error: null });
    const { limit } = this.state;
    const chapurPagination = 'https://chapurmx.wcaas.net/api/wcaas/mongoApi/chapurmx/page/productsByCategory/';
    const category = 'moda-mujer/ropa/blusas-y-playeras';
    const paginate = `${chapurPagination}${category}?lang=es_mx&currency=mxn&page=${page}&limit=${limit}`;
    try {
      const url = paginate;
      const req = await fetch(url);
      const res = await req.json();
      const items = res.productData;
      const totalItems = parseFloat(res.totalProducts.replace(',', ''));
      const pages = (totalItems / limit);
      const totalPages = (pages > pages.toFixed()) ? pages.toFixed() + 1 : pages.toFixed();
      this.setState({ data: res, products: items, pages: parseFloat(totalPages) });
    } catch (err) {
      this.setState({ error: err });
    } finally {
      console.log('pageRequest is done -- ', this.state);
    }
  }

  paginatePrev = async () => {
    const { data, page, returned, products } = this.state;
    const prev = (page - 1);
    const newItems = products !== [] ? products : [];
    const returnedItems = returned - parseFloat(data.returnedProducts);
    if (page > 1) {
      this.setState({ page: prev, returned: returnedItems, products: newItems });
      this.pageRequest(prev);
    }
  }

  paginateNext = async () => {
    const { data, page, pages, returned, products } = this.state;
    const next = (page + 1);
    const newItems = products !== [] ? products : [];
    const returnedItems = returned + parseFloat(data.returnedProducts);
    const lastPage = page === pages;
    // const totalPages = (totalProducts / limit);
    if (!lastPage) {
      this.setState({ page: next, returned: returnedItems, products: newItems });
      this.pageRequest(next);
    }
  }

  render() {
    const { data, page, pages, returned, products, limit, error } = this.state;
    const cdn = 'https://cdn.chapur.com.mx';

    const err = error !== null ? <h4>{error}</h4> : '';
    const ready = data !== null && products !== null;
    const totalProducts = ready && data.totalProducts ? parseFloat(data.totalProducts.replace(',', '')) : 'loading...';
    let totalReturned;
    if (returned > totalProducts) {
      totalReturned = (returned - (returned - totalProducts));
    } else { totalReturned = returned; }
    const totalPages = (totalProducts / limit);
    const pageCount = `${page} / ${totalPages > parseFloat(totalPages.toFixed()) ? parseFloat(totalPages.toFixed()) + 1 : parseFloat(totalPages.toFixed())}`;

    const prevClassname = (page === 1) ? 'disabled' : '';
    const nextClassname = (page === pages) ? 'disabled' : '';

    const list = ready ? products.map(i => (
      <article key={i.part_number} className="pagination--item">
        <picture>
          <img src={`${cdn}/${i.part_number}/${i.items[0].image_correlator}_1_500x500.jpg`} alt={i.items[0].locales.es_mx.name} />
        </picture>
        <header>
          <h3>{i.items[0].locales.es_mx.name}</h3>
          <p>{`$${i.items[0].pricing.mxn.list_price / 100}.00`}</p>
        </header>
      </article>
    )) : <PlaceholderList limit={limit} />;

    return (
      <main>
        <header className="pagination--header">
          <h1>Catalog Pagination</h1>
          <div>
            <p>currentPage: </p>
            <b>{pageCount}</b>
          </div>
          <div>
            <p>totalProducts: </p>
            <b>{totalProducts}</b>
          </div>
          <div>
            <p>returnedProducts: </p>
            <b>{totalReturned}</b>
          </div>
        </header>
        <div className="pagination--nav">
          <a className={prevClassname} onClick={this.paginatePrev}> « Prev </a>
          <b>{pageCount}</b>
          <a className={nextClassname} onClick={this.paginateNext}> Next » </a>
        </div>
        {err}
        <section className="pagination--content">{list}</section>
      </main>
    );
  }
}

export default Pagination;
