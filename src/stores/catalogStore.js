// Pagination
export const pageRequest = async (page, limit) => {
  const merchant = 'muebiliamx';
  const origin = `https://${merchant}.wcaas.net/api/wcaas/mongoApi/${merchant}/page/productsByCategory/`;
  const category = 'estudio';
  const paginate = `${origin}${category}?lang=es_mx&currency=mxn&page=${page}&limit=${limit}`;
  try {
    const url = paginate;
    const req = await fetch(url);
    const res = await req.json();
    console.log('---* pageRequest res: ', res);
  } catch (err) {
    console.log('---* pageRequest err: ', err);
  } finally {
    console.log('pageRequest success -- ');
  }
};

export default pageRequest;
