export const categoryStore = async () => {
  const merchant = 'muebiliamx';
  const origin = `https://${merchant}.wcaas.net/api/wcaas/mongoApi/${merchant}/categories/`;
  try {
    const url = origin;
    const req = await fetch(url);
    const res = await req.json();
    console.log('---* categoryRequest res: ', res);
  } catch (err) {
    console.log('---* categoryRequest err: ', err);
  } finally {
    console.log('categoryRequest success -- ');
  }
};

export default categoryStore;
